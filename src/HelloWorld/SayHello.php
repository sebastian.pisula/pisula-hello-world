<?php

namespace HelloWorld;

class SayHello {
	public static function world() {
		return __('Hello World, Composer!', 'test-app');
	}
}